<html>
    <head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
        <script src="js/script.js"></script>
    </head>
    <body onload="startTime()">
        <header class="shadow-box">
				<a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
		</header>
        <div class="welcome">
            <p><h4 class="pWelcome" id="errorLogin">Bine ati venit! PSQuiz este un site care incearca sa iti testeze cunostintele. Pentru a putea rezolva intrebarile noastre trebuie sa completezi formularul de logare de mai jos folosind Username si Password, dar numai daca te-ai inregistrat. Daca nu ai cont apasa pe butonul de Register!<br><br>
            <div id="txt"></div>
            </h4> 
        </div>
        <div class="divLogin">
            <h3>Login:</h3>    
            <form name="login" action="Login">
                <label for="username">
                    Username</label> <input class="inputUser" type="username" id="usename" name="username" required="required"><br><br>
                <label for="password">
                    Password:</label> <input class="inputPass" type="password" id="password" name="password" required="required"><br><br>
                <button type = "submit" name="submit">Login</button><br><br><br><br>
            </form>
            <a href = "signup.jsp" name="register" type="submit" class="divLogin" >Register</a><br><br>
        </div>
    </body>
    <br><br><br><br><br>
    <footer id="foot" class="bottom-page">
        <h2>&copy; 2017 Paul Soldan</h2>
    </footer>
</html>