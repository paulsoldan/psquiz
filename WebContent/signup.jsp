<html>
    <head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
        <script src="js/script.js"></script>
        <style>
			p.message0{color:red;}
			p.message1{color:green;}
		</style>
    </head>
    <body onload="startTime()">
        <header class="shadow-box">
				<a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
		</header>
        <div class="welcome">
            <p><h4 class="pWelcome" id="errorLogin">Bine ati venit! PSQuiz este un site care incearca sa iti testeze cunostintele. Pentru a putea rezolva intrebarile noastre trebuie sa completezi formularul de inregistrare de mai jos folosind Username si Password iar apoi click pe butonul LOGIN pentru urmatoarele instructiuni.<br><br>
            <div id="txt"></div>
            </h4> 
        </div>
        <div class="divLogin">
            <h3>Sign Up:</h3>    
            <form name="login" action="SignUp" method="post">
                <label for="username">
                    Username</label> <input class="inputUser" type="username" id="usename" name="username" required="required"><br><br>
                <label for="password">
                    Password:</label> <input class="inputPass" type="password" id="password" name="password" required="required"><br><br>
                <button type = "submit" name="submit">Register</button><br><br> 
                <p class="message1" style="font-family: 'halfmoon';color=#FF0000;">${message1}</p>	
				<p class="message0" style="font-family: 'halfmoon';color=#00FF00;">${message0}</p>	 
            </form>
            
            <a href = "index.jsp" name="login" type="submit" class="divLogin">Login</a><br><br>
        </div>
    </body>
    <br><br><br><br><br>
    <footer id="foot" class="bottom-page">
        <h2>&copy; 2017 Paul Soldan</h2>
    </footer>
</html>