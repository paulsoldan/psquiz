<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
</head>
<%
if(session.getAttribute("username")==null)
{
	response.sendRedirect("/bdproiect/index.jsp");
}
%>   
<header class="shadow-box">
    <a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
    <form name="login" action="Logout">
        <input type="submit" name="submit" value="Deconectare" class="button-link" />
    </form>   
</header>


<body>
    <br><br><br><br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
		      <h1>Administreaza pagina!</h1>
               <br></center>
	       </div>
        </div>
	</div>
	<br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
					<form action="Profile" method="post" id="quiz">
						
				            <ol >
				            
				            	<%
				            		Integer value = (Integer)request.getAttribute("total");
				            		
				            		List<String> questions = new ArrayList<String>();
				            		List<String> a = new ArrayList<String>();
				            		List<String> b = new ArrayList<String>();
				            		List<String> c = new ArrayList<String>();
				            		questions = (List<String>)request.getAttribute("quizTitle_list");
				            		a = (List<String>)request.getAttribute("quizQuestionA_list");
				            		b = (List<String>)request.getAttribute("quizQuestionB_list");
				            		c = (List<String>)request.getAttribute("quizQuestionC_list");
				            		for(int i=0;i<value;i++)
				            		{
				            			request.setAttribute("id",i);
				            			%>
						                <li>
						                    <h3><%out.println(questions.get(i));%></h3>
						                    
						                    <div>
						                        <input type="radio" name="question-${id}-answers" id="question-${id}-answers-A" value="A" />
						                        <label for="question-${id}-answers-A"> <%out.println(a.get(i));%> </label>
						                    </div>
						                    
						                    <div>
						                        <input type="radio" name="question-${id}-answers" id="question-${id}-answers-B" value="B" />
						                        <label for="question-${id}-answers-B"> <%out.println(b.get(i));%></label>
						                    </div>
						                    
						                    <div>
						                        <input type="radio" name="question-${id}-answers" id="question-${id}-answers-C" value="C" />
						                        <label for="question-${id}-answers-C"> <%out.println(c.get(i));%></label>
						                    </div>
						                
						                </li>
				                		<%
				                	}	
				            	%>
				            </ol>
					</form>
	         <br></center>
	       </div>
        </div>
	</div>
	<br><br>
	<div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
           		<center>
	           		<br>
					<p><h3>Daca doresti sa adaugi o intrebare apasa butonul "Adauga Intrebare" de mai jos.</h3>
					<form action="/bdproiect/adauga.jsp">
						<input type="submit" name="submit" id="adauga" value="Adauga intrebare">
					</form>
					<p><h3>Pentru a modifica sau sterge o intrebare, selecteaza numarul intrebarii apoi apasa unul din butoanele "Modifica Intrebare" sau "Sterge Intrebare".</h3>
					<form action="Sterge" method="post">
						<input type="number" name="quantity" min="1" max="${nrMax}">
						<input type="submit" name="submit" id="sterge" value="Sterge Intrebare">
					</form>
					<form action="Modifica" method="get">
						<input type="number" name="quantity1" min="1" max="${nrMax}">
						<input type="submit" name="submit" id="editeaza" value="Modifica Intrebare">
					</form>
					<br><br>
				</center>
			</div>
        </div>
	</div>
</body>

<br><br><br><br>

<br><br><br><br>
<footer id="foot" class="bottom-page">
    <h2>&copy; 2017 Paul Soldan</h2>
</footer>