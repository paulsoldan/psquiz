<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
</head>
<%
if(session.getAttribute("username")==null)
{
	response.sendRedirect("/bdproiect/index.jsp");
}
%>   
<header class="shadow-box">
    <a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
    <form name="login" action="Logout">
        <input type="submit" name="submit" value="Deconectare" class="button-link" />
    </form>   
</header>


<body>
    <br><br><br><br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
		      <h1>Administreaza pagina!</h1>
               <br></center>
	       </div>
        </div>
	</div>
	<br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
				<p><h3>Titlul intrebarii:</h3>
				<form action="Modifica" method="post">
				  <input type="text" name="intrebare" value="${intrebare}">
				  <p><h3>Raspuns A:</h3>
				  <input type="text" name="raspunsA" value="${raspunsA}">
				  <p><h3>Raspuns B:</h3>
				  <input type="text" name="raspunsB" value="${raspunsB}">
				  <p><h3>Raspuns C:</h3>
				  <input type="text" name="raspunsC" value="${raspunsC}">
				  <p><h3>Varianta corecta (completati cu "a", "b" sau "c"):</h3>
				  <input type="text" name="variantaCorecta" value="${variantaCorecta}">
				  <br>
	         <br></center>
	       </div>
        </div>
	</div>
	<br><br>
	<div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
           		<center>
	           		<br>
	           			<form action="Modifica" method="post">
				  			<input type="submit" name="submit" id="sterge" value="Modifica intrebarea!">
						</form>
					<br>
				</center>
			</div>
        </div>
	</div>
</body>

<br><br><br><br>

<br><br><br><br>
<footer id="foot" class="bottom-page">
    <h2>&copy; 2017 Paul Soldan</h2>
</footer>