<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
        <script src="js/script.js"></script>
</head>
<header class="shadow-box">
    <a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
    <form name="login" action="Logout">
        <input type="submit" name="submit" value="Deconectare" class="button-link" />
    </form>   
</header>
<br><br><br><br>
<%
if(session.getAttribute("username")==null)
{
	response.sendRedirect("/bdproiect/index.jsp");
}
%>
<body>

	<div id="page-wrap" class="quiz-style">
        
		<div class="div_quiz">
            <br />
            <center><h1>Cultura generala</h1></center>
		
		<form action="Profile" method="post" id="quiz">
		
            <ol >
            
            	<%
            		Integer value = (Integer)request.getAttribute("total");
            		
            		List<String> questions = new ArrayList<String>();
            		List<String> a = new ArrayList<String>();
            		List<String> b = new ArrayList<String>();
            		List<String> c = new ArrayList<String>();
            		questions = (List<String>)request.getAttribute("quizTitle_list");
            		a = (List<String>)request.getAttribute("quizQuestionA_list");
            		b = (List<String>)request.getAttribute("quizQuestionB_list");
            		c = (List<String>)request.getAttribute("quizQuestionC_list");
            		for(int i=0;i<value;i++)
            		{
            			request.setAttribute("id",i);
            			%>
		                <li>
		                    <h3><%out.println(questions.get(i));%></h3>
		                    
		                    <div>
		                        <input type="radio" name="question-${id}-answers-A" id="question-${id}-answers-A" value="A" />
		                        <label for="question-${id}-answers-A"> <%out.println(a.get(i));%> </label>
		                    </div>
		                    
		                    <div>
		                        <input type="radio" name="question-${id}-answers-B" id="question-${id}-answers-B" value="B" />
		                        <label for="question-${id}-answers-B"> <%out.println(b.get(i));%></label>
		                    </div>
		                    
		                    <div>
		                        <input type="radio" name="question-${id}-answers-C" id="question-${id}-answers-C" value="C" />
		                        <label for="question-${id}-answers-C"> <%out.println(c.get(i));%></label>
		                    </div>
		                
		                </li>
                		<%
                	}	
            	%>
            
            </ol>
            
            <center><input type="submit" value="Afla raspunsul!" class="submit-quiz" /></center>
            <br />
		
		</form>
    </div>
	</div>
	

</body>
<br><br><br><br>
<footer id="foot" class="bottom-page">
    <h2>&copy; 2017 Paul Soldan</h2>
</footer>
