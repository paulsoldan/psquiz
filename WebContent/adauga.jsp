<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8" />
        <title>PS Quiz</title>
        <link rel="shortcut icon" href="img/logoSigla.png" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" /> 
</head>
<%
if(session.getAttribute("username")==null)
{
	response.sendRedirect("/bdproiect/index.jsp");
}
%>   
<header class="shadow-box">
    <a href="index.jsp" id="logo-img" class="logo-img"><img src="img/logo.png" alt="logo" class="logo" width="auto" height="50"></a>
    <form name="login" action="Logout">
        <input type="submit" name="submit" value="Deconectare" class="button-link" />
    </form>   
</header>


<body>
    <br><br><br><br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
		      <h1>Administreaza pagina!</h1>
               <br></center>
	       </div>
        </div>
	</div>
	<br><br>
    <div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
               <center><br>
				<p><h3>Titlul intrebarii:</h3>
				<form action="Adauga" method="post">
				  <textarea name="intrebare" rows="10" cols="30"></textarea>
				  <p><h3>Raspuns A:</h3>
				  <textarea name="raspunsA" rows="10" cols="30"></textarea>
				  <p><h3>Raspuns B:</h3>
				  <textarea name="raspunsB" rows="10" cols="30"></textarea>
				  <p><h3>Raspuns C:</h3>
				  <textarea name="raspunsC" rows="10" cols="30"></textarea>
				  <p><h3>Varianta corecta (completati cu "a", "b" sau "c"):</h3>
				  <textarea name="variantaCorecta" rows="10" cols="30"></textarea>
				  <br>
	         <br></center>
	       </div>
        </div>
	</div>
	<br><br>
	<div class="quiz-style">
	   <div id="page-wrap">
           <div class="div_quiz">
           		<center>
	           		<br>
	           			<form action="Adauga" method="post">
				  			<input type="submit" name="submit" id="sterge" value="Adauga intrebarea!">
						</form>
					<br>
				</center>
			</div>
        </div>
	</div>
</body>

<br><br><br><br>

<br><br><br><br>
<footer id="foot" class="bottom-page">
    <h2>&copy; 2017 Paul Soldan</h2>
</footer>