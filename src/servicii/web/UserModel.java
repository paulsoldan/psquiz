package servicii.web;

public class UserModel {
	private int user_id;
	private String username;
	private String password;
	
	
	public UserModel(int user_id, String username, String password)
	{
		this.user_id=user_id;
		this.username=username;
		this.password=password;
		
	}
	public int getUser_id()
	{
		return user_id;
	}
	public String getUsername()
	{
		return username;
	}
	public String getPassword()
	{
		return password;
	}
}
