package servicii.web;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.security.*;
import java.math.*;
public class QuizManager {
	public static String messageError="Wrong password or e-mail address. Please try again!";
	private static final String URL = "jdbc:mysql://localhost:3306/quiz";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "soldan1234";
	
	private static final QuizManager instance = new QuizManager();
	private Connection conn;
	public String feedUser;
	public int tot;
	int idGlob;
	public static QuizManager getInstance() {
		return instance;
	}
	private QuizManager() {
		System.out.println("Loading driver...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException(
					"Cannot find the driver in the classpath!", e);
		}
		try {
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public List<UserModel> getUserList() {
		try (Statement st = conn.createStatement()) {
			List<UserModel> UserList = new ArrayList<UserModel>();
			st.execute("select * from users");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				UserModel User = new UserModel(rs.getInt("user_id"),
					rs.getString("username"), rs.getString("password"));
				UserList.add(User);
			}
			return UserList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String md5(String data){
		try{
			MessageDigest md=MessageDigest.getInstance("MD5");
			byte[] messageDigest=md.digest(data.getBytes());
			BigInteger num=new BigInteger(1,messageDigest);
			String hashtext=num.toString(16);
			while(hashtext.length()<32){
				hashtext="0"+hashtext;
			}
			return hashtext;
		}catch(NoSuchAlgorithmException e){
			throw new RuntimeException(e);
		}
	}
	public List<String> testLogin(String user_name,String user_password){
		try(Statement st=conn.createStatement())
		{
			List<String> user_list=new ArrayList<String>();
			st.execute(("select * from users where username='")+user_name+("' && password='")+(user_password)+("';"));
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				user_list.add(rs.getString("user_id"));
				user_list.add(rs.getString("username"));
				user_list.add(rs.getString("password"));
			}
			if(!user_list.isEmpty())
				feedUser=user_list.get(1);
			return user_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> testAdminLogin(String user_name,String user_password){
		try(Statement st=conn.createStatement())
		{
			List<String> user_list=new ArrayList<String>();
			st.execute(("select * from admin where admin_username='")+user_name+("' && admin_password='")+(user_password)+("';"));
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				user_list.add(rs.getString("admin_id"));
				user_list.add(rs.getString("admin_username"));
				user_list.add(rs.getString("admin_password"));
			}
			
			return user_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public int signUp(String user_name,String user_password){
		try(Statement st=conn.createStatement())
		{ 
			st.execute("select username from users where username='"+user_name+"';");
			ResultSet rs=st.getResultSet();
			if(rs.next()){
				System.out.println("exista");
				return 0;
			}
			else
			{
				st.execute("insert into users(username,password) values(\""+user_name+'"'+','+'"'+(user_password)+'"'+");");
				System.out.println("nu exista");
				return 1;
			}
		}catch (SQLException e)
		{
			e.printStackTrace();
			System.out.println(e);
			return 0;
		}
	}
	public boolean deleteUserList(){
		try(Statement st=conn.createStatement())
		{
			return st.execute("delete * from users");
			
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}	
	}
	public int countRowQuizTitle(){
		try(Statement st=conn.createStatement())
		{
			int count=0;
			st.execute("select count(*) from quiz_title;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				count=rs.getInt("count(*)");
			}
			System.out.print("nr de intrebari:");
			System.out.println(count);
			tot=count;
			return count;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	public List<String> quizId(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_id_list=new ArrayList<String>();
			st.execute("select quiz_id from quiz_title;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_id_list.add(rs.getString("quiz_id"));
			}
			
			return quiz_id_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizTitle(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_title_list=new ArrayList<String>();
			st.execute("select title from quiz_title;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_title_list.add(rs.getString("title"));
			}
			
			return quiz_title_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizQuestionId(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_question_id_list=new ArrayList<String>();
			st.execute("select quiz_id_option from quiz_question_option;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_question_id_list.add(rs.getString("quiz_id_option"));
			}
			
			return quiz_question_id_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizQuestionA(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_question_a_list=new ArrayList<String>();
			st.execute("select a from quiz_question_option;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_question_a_list.add(rs.getString("a"));
			}
			
			return quiz_question_a_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizQuestionB(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_question_b_list=new ArrayList<String>();
			st.execute("select b from quiz_question_option;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_question_b_list.add(rs.getString("b"));
			}
			
			return quiz_question_b_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizQuestionC(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_question_c_list=new ArrayList<String>();
			st.execute("select c from quiz_question_option;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_question_c_list.add(rs.getString("c"));
			}
			
			return quiz_question_c_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> quizQuestionIsCorrect(){
		try(Statement st=conn.createStatement())
		{
			List<String> quiz_question_iscorrect_list=new ArrayList<String>();
			st.execute("select is_correct from quiz_question_option;");
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				quiz_question_iscorrect_list.add(rs.getString("is_correct"));
			}
			
			return quiz_question_iscorrect_list;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public void insertFeedback(String feedback){
		try(Statement st=conn.createStatement())
		{
			st.execute("select * from feedback where username='"+feedUser+"';");
			ResultSet rs=st.getResultSet();
			if(rs.next()){
				System.out.println("exista");
				st.execute("update feedback set text='"+feedback+"' where username='"+feedUser+"';");
			}
			else
			{
				System.out.println("nu exista");
				st.execute("insert into feedback(username,text) values (\""+feedUser+'"'+','+'"'+feedback+'"'+");");
			}	
		} catch (SQLException e)
		{
			e.printStackTrace();
			return;
		}	
	}
	public void adaugaIntrebare(String text,String varA,String varB,String varC,String varCor){
		try(Statement st=conn.createStatement())
		{
			st.execute("SELECT * FROM quiz_question_option ORDER BY quiz_id_option DESC LIMIT 1;");
			ResultSet rs=st.getResultSet();
			int id=0;
			while(rs.next())
			{
				id=rs.getInt(1);
			}
			st.execute("insert into quiz_title(quiz_id,title) values (\""+(id+1)+'"'+','+'"'+text+'"'+");");
			st.execute("insert into quiz_question_option(quiz_id_option,a,b,c,is_correct) values (\""+(id+1)+'"'+','+'"'+varA+'"'+','+'"'+varB+'"'+','+'"'+varC+'"'+','+'"'+varCor+'"'+");");
			
		} catch (SQLException e)
		{
			e.printStackTrace();
			return;
		}	
	}
	public void stergeIntrebare(int nrIntrebare){
		try(Statement st=conn.createStatement())
		{
			st.execute("SELECT * FROM quiz_question_option LIMIT "+(nrIntrebare-1)+",1");
			ResultSet rs=st.getResultSet();
			int id=0;
			while(rs.next())
			{
				id=rs.getInt(1);
			}
			st.execute("delete from quiz_question_option where quiz_id_option="+id);
			st.execute("delete from quiz_title where quiz_id="+id);
			
		} catch (SQLException e)
		{
			e.printStackTrace();
			return;
		}	
	}
	public void modificaIntrebare(String titlu,String a,String b,String c,String varCorecta){
		try(Statement st=conn.createStatement())
		{
			st.execute("update quiz_question_option set a='"+a+"', b='"+b+"', c='"+c+"',is_correct='"+varCorecta+"' where quiz_id_option="+idGlob+";");
			st.execute("update quiz_title set title='"+titlu+"' where quiz_id="+idGlob+";");
		} catch (SQLException e)
		{
			e.printStackTrace();
			return;
		}	
	}
	public List<String> scoateIntrebare(int nrIntrebare){
		try(Statement st=conn.createStatement())
		{
			st.execute("SELECT * FROM quiz_question_option LIMIT "+(nrIntrebare-1)+",1");
			ResultSet rs1=st.getResultSet();
			int id=0;
			while(rs1.next())
			{
				id=rs1.getInt(1);
			}
			List<String> intrebare=new ArrayList<String>();
			st.execute(("select * from quiz_title where quiz_id="+id));
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				intrebare.add(rs.getString("quiz_id"));
				intrebare.add(rs.getString("title"));
			}
			return intrebare;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public List<String> scoateRaspuns(int nrIntrebare){
		try(Statement st=conn.createStatement())
		{
			st.execute("SELECT * FROM quiz_question_option LIMIT "+(nrIntrebare-1)+",1");
			ResultSet rs1=st.getResultSet();
			int id=0;
			while(rs1.next())
			{
				id=rs1.getInt(1);
			}
			idGlob=id;
			List<String> raspuns=new ArrayList<String>();
			st.execute(("select * from quiz_question_option where quiz_id_option="+id));
			ResultSet rs=st.getResultSet();
			while(rs.next()){
				raspuns.add(rs.getString("quiz_id_option"));
				raspuns.add(rs.getString("a"));
				raspuns.add(rs.getString("b"));
				raspuns.add(rs.getString("c"));
				raspuns.add(rs.getString("is_correct"));
			}
			return raspuns;
		}catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}