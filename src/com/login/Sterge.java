package com.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Sterge
 */
@WebServlet("/Sterge")
public class Sterge extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String nrIntrebare;
		nrIntrebare=request.getParameter("quantity");
		int nr = Integer.parseInt(nrIntrebare);
		QuizManager.getInstance().stergeIntrebare(nr);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/sterge.jsp");
        dispatcher.forward(request,response);
	}

}
