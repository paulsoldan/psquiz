package com.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xml.internal.security.utils.SignerOutputStream;

import java.util.List;
import servicii.web.QuizManager;

@WebServlet("/Login")
public class Login extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username =request.getParameter("username");
		String password =request.getParameter("password");
		//System.out.println(username);
		//System.out.println(password);
		List<String> userList = QuizManager.getInstance().testLogin(username, password);
		List<String> adminList = QuizManager.getInstance().testAdminLogin(username, password);
		//System.out.println(userList.get(1));
		//System.out.println(userList.get(2));
		if(!adminList.isEmpty())
		{
			if(username.equals(adminList.get(1)) && password.equals(adminList.get(2))){
				HttpSession session=request.getSession();
				session.setAttribute("username", username);
				RequestDispatcher rd = request.getRequestDispatcher("Admin");
				rd.forward(request,response);
			}
		}
		else
		{
			if(!userList.isEmpty()){
				if(username.equals(userList.get(1)) && password.equals(userList.get(2))){
					HttpSession session=request.getSession();
					session.setAttribute("username", username);
					RequestDispatcher rd = request.getRequestDispatcher("Profile");
					rd.forward(request,response);
				}
				else
				{
					response.sendRedirect("/bdproiect/signup.jsp");
				}
			}
			else
			{
				response.sendRedirect("/bdproiect/signup.jsp");
			}
		}
	}
}