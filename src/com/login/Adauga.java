package com.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Adauga
 */
@WebServlet("/Adauga")
public class Adauga extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String titlu,raspunsa,raspunsb,raspunsc,varCorecta;
		titlu=request.getParameter("intrebare");
		raspunsa=request.getParameter("raspunsA");
		raspunsb=request.getParameter("raspunsB");
		raspunsc=request.getParameter("raspunsC");
		varCorecta=request.getParameter("variantaCorecta");
		QuizManager.getInstance().adaugaIntrebare(titlu, raspunsa, raspunsb, raspunsc, varCorecta);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adaugaFinal.jsp");
        dispatcher.forward(request,response);
	}

}
