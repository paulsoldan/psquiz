package com.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Verificare
 */
@WebServlet("/Verificare")
public class Verificare extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String feedback;
		feedback=request.getParameter("message");
		QuizManager.getInstance().insertFeedback(feedback);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/final.jsp");
        dispatcher.forward(request,response);
	}
}
