package com.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import servicii.web.QuizManager;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	public int result;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(result==1)
		{	
			String message="Succes register!";
	        request.setAttribute("message", message); // This will be available as ${message}
	        request.getRequestDispatcher("/signup.jsp").forward(request, response);
	        System.out.println("Succes register!");
		}
		else
		{
			String message="Try again register!";
			request.setAttribute("message", message); // This will be available as ${message}
	        request.getRequestDispatcher("/signup.jsp").forward(request, response);
	        System.out.println("Try again register!");
		}	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		String username,password;
		username=request.getParameter("username").toString();
		password=request.getParameter("password").toString();
		System.out.println(username);
		System.out.println(password);
		result=QuizManager.getInstance().signUp(username,password);
		if(result==1)
		{
			String message1 = "You are Successfully Registered "+username+"! Please login to access your Profile!";
	        request.setAttribute("message1", message1); // This will be available as ${message}
	        request.getRequestDispatcher("/signup.jsp").forward(request, response);
		}
		else
		{
			String message0 = "Oops! Error. Please try again later!!!";
	        request.setAttribute("message0", message0); // This will be available as ${message}
	        request.getRequestDispatcher("/signup.jsp").forward(request, response);
		}	
	}
}
