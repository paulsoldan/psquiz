package com.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		int total=0;
		List<String> quizId_list=new ArrayList<String>();
		List<String> quizTitle_list=new ArrayList<String>();
		List<String> quizQuestionsId_list=new ArrayList<String>();
		List<String> quizQuestionA_list=new ArrayList<String>();
		List<String> quizQuestionB_list=new ArrayList<String>();
		List<String> quizQuestionC_list=new ArrayList<String>();
		List<String> quizQuestionIsCorrect_list=new ArrayList<String>();
		total=QuizManager.getInstance().countRowQuizTitle();
		quizId_list=QuizManager.getInstance().quizId();
		quizTitle_list=QuizManager.getInstance().quizTitle();
		quizQuestionsId_list=QuizManager.getInstance().quizQuestionId();
		quizQuestionA_list=QuizManager.getInstance().quizQuestionA();
		quizQuestionB_list=QuizManager.getInstance().quizQuestionB();
		quizQuestionC_list=QuizManager.getInstance().quizQuestionC();
		quizQuestionIsCorrect_list=QuizManager.getInstance().quizQuestionIsCorrect();
		request.setAttribute("quizId_list",quizId_list);
		request.setAttribute("quizTitle_list",quizTitle_list);
		request.setAttribute("quizQuestionsId_list",quizQuestionsId_list);
		request.setAttribute("quizQuestionA_list",quizQuestionA_list);
		request.setAttribute("quizQuestionB_list",quizQuestionB_list);
		request.setAttribute("quizQuestionC_list",quizQuestionC_list);
		request.setAttribute("quizQuestionIsCorrect_list",quizQuestionIsCorrect_list);
		request.setAttribute("total",total);
		for(int i=0;i<total;i++){
			request.setAttribute("quiz",quizTitle_list.get(i));
			request.setAttribute("a",quizQuestionA_list.get(i));
			request.setAttribute("b",quizQuestionB_list.get(i));
			request.setAttribute("c",quizQuestionC_list.get(i));
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin.jsp");
        dispatcher.forward(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
