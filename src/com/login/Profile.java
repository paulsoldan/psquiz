package com.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		int total=0;
		List<String> quizId_list=new ArrayList<String>();
		List<String> quizTitle_list=new ArrayList<String>();
		List<String> quizQuestionsId_list=new ArrayList<String>();
		List<String> quizQuestionA_list=new ArrayList<String>();
		List<String> quizQuestionB_list=new ArrayList<String>();
		List<String> quizQuestionC_list=new ArrayList<String>();
		List<String> quizQuestionIsCorrect_list=new ArrayList<String>();
		total=QuizManager.getInstance().countRowQuizTitle();
		quizId_list=QuizManager.getInstance().quizId();
		quizTitle_list=QuizManager.getInstance().quizTitle();
		quizQuestionsId_list=QuizManager.getInstance().quizQuestionId();
		quizQuestionA_list=QuizManager.getInstance().quizQuestionA();
		quizQuestionB_list=QuizManager.getInstance().quizQuestionB();
		quizQuestionC_list=QuizManager.getInstance().quizQuestionC();
		quizQuestionIsCorrect_list=QuizManager.getInstance().quizQuestionIsCorrect();
		request.setAttribute("quizId_list",quizId_list);
		request.setAttribute("quizTitle_list",quizTitle_list);
		request.setAttribute("quizQuestionsId_list",quizQuestionsId_list);
		request.setAttribute("quizQuestionA_list",quizQuestionA_list);
		request.setAttribute("quizQuestionB_list",quizQuestionB_list);
		request.setAttribute("quizQuestionC_list",quizQuestionC_list);
		request.setAttribute("quizQuestionIsCorrect_list",quizQuestionIsCorrect_list);
		request.setAttribute("total",total);
		/*for(int i=0;i<total;i++){
			request.setAttribute("quiz"+i,quizTitle_list.get(i));
			request.setAttribute("a"+i,quizQuestionA_list.get(i));
			request.setAttribute("b"+i,quizQuestionB_list.get(i));
			request.setAttribute("c"+i,quizQuestionC_list.get(i));
		}*/
		/*String v[][]=new String[total][4];
		for(int i=0;i<total;i++){
			for(int j=0;j<4;j++)
			{
				v[i][j]=
			}
		}*/
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/intrebari.jsp");
        dispatcher.forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		int total=0;
		/*List<String> quizId_list=new ArrayList<String>();
		List<String> quizTitle_list=new ArrayList<String>();
		List<String> quizQuestionsId_list=new ArrayList<String>();
		List<String> quizQuestionA_list=new ArrayList<String>();
		List<String> quizQuestionB_list=new ArrayList<String>();
		List<String> quizQuestionC_list=new ArrayList<String>();*/
		List<String> quizQuestionIsCorrect_list=new ArrayList<String>();
		total=QuizManager.getInstance().countRowQuizTitle();
		/*quizId_list=QuizManager.getInstance().quizId();
		quizTitle_list=QuizManager.getInstance().quizTitle();
		quizQuestionsId_list=QuizManager.getInstance().quizQuestionId();
		quizQuestionA_list=QuizManager.getInstance().quizQuestionA();
		quizQuestionB_list=QuizManager.getInstance().quizQuestionB();
		quizQuestionC_list=QuizManager.getInstance().quizQuestionC();
		*/
		quizQuestionIsCorrect_list=QuizManager.getInstance().quizQuestionIsCorrect();
		/*request.setAttribute("quizId_list",quizId_list);
		request.setAttribute("quizTitle_list",quizTitle_list);
		request.setAttribute("quizQuestionsId_list",quizQuestionsId_list);
		request.setAttribute("quizQuestionA_list",quizQuestionA_list);
		request.setAttribute("quizQuestionB_list",quizQuestionB_list);
		request.setAttribute("quizQuestionC_list",quizQuestionC_list);
		request.setAttribute("quizQuestionIsCorrect_list",quizQuestionIsCorrect_list);
		System.out.println(quizId_list.get(0));
		System.out.println(quizTitle_list.get(0));*/
		request.setAttribute("total",total);
		int counter=0;
		for(int i=0;i<total;i++){
			switch (quizQuestionIsCorrect_list.get(i))
			{
				case "a":
					if ("A".equals((String)request.getParameter("question-"+i+"-answers-A")))
						counter++;
					break;
				case "b":
					if ("B".equals((String)request.getParameter("question-"+i+"-answers-B")))
						counter++;
					break;
				case "c":
					if ("C".equals((String)request.getParameter("question-"+i+"-answers-C")))
						counter++;
					break;
			}
		}
		request.setAttribute("counter",counter);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/verificare.jsp");
        dispatcher.forward(request,response);
	}
}
