package com.login;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicii.web.QuizManager;

/**
 * Servlet implementation class Modifica
 */
@WebServlet("/Modifica")
public class Modifica extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String nrIntrebare;
		nrIntrebare=request.getParameter("quantity1");
		int nr = Integer.parseInt(nrIntrebare);
		List<String> title = QuizManager.getInstance().scoateIntrebare(nr);
		List<String> options = QuizManager.getInstance().scoateRaspuns(nr);
		request.setAttribute("intrebare", title.get(1));
		request.setAttribute("raspunsA", options.get(1));
		request.setAttribute("raspunsB", options.get(2));
		request.setAttribute("raspunsC", options.get(3));
		request.setAttribute("variantaCorecta", options.get(4));
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/modifica.jsp");
        dispatcher.forward(request,response);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String intrebare,a,b,c,isCorrect;
		intrebare=request.getParameter("intrebare");
		a=request.getParameter("raspunsA");
		b=request.getParameter("raspunsB");
		c=request.getParameter("raspunsC");
		isCorrect=request.getParameter("variantaCorecta");
		QuizManager.getInstance().modificaIntrebare(intrebare, a, b, c, isCorrect);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/modificaFinal.jsp");
        dispatcher.forward(request,response);
	}

}
